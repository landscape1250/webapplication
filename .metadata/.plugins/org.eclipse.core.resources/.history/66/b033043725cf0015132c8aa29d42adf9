package com.apple.webapp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.apple.webapp.dao.UserDAO;
import com.apple.webapp.model.User;

@Repository("userDAO")
@Transactional
public class UserDAOImpl implements UserDAO {

	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(user);
		logger.info("User saved successfully, User Details=" + user);
	}

	@Override
	public User findUserByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, name);
		logger.info("User loaded successfully, User details=" + user);
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUsers() {
		Session session = this.sessionFactory.getCurrentSession();
		List<User> usersList = session.createQuery("from t_user").list();
		for (User user : usersList) {
			logger.info("User List::" + user);
		}
		return usersList;
	}

	@Override
	public User updateUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(user);
		logger.info("User updated successfully, User Details=" + user);
		return user;
	}

	@Override
	public void deleteUserByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, name);
		if (null != user) {
			session.delete(user);
		}
		logger.info("User deleted successfully, user details=" + user);
	}

}
