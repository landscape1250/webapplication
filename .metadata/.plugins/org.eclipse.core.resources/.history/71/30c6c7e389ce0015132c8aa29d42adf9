package com.apple.webapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.apple.webapp.model.User;
import com.apple.webapp.service.UserService;

/**
 * Handles requests for the application home page.
 */

@RequestMapping("/")
@Controller
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired(required = true)
	private UserService userService;

	/**
	 * Create a User
	 */
	@RequestMapping(value = "/user/", method = RequestMethod.POST)
	public ResponseEntity<User> createUser(@RequestBody User user) {
		logger.info("Creating User " + user.getName());
		if (!userService.isUserExist(user)) {
			userService.saveUser(user);
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} else {
			if (logger.isInfoEnabled()) {
				logger.info("Sorry, User " + user.getName() + " already exist");
			}
			return new ResponseEntity<User>(HttpStatus.CONFLICT);
		}
	}

	/**
	 * Find One User By Id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> findUserById(@PathVariable("id") Long id) {
		if (id == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Please input valid id.");
			}
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		User user = userService.findUserById(id);
		if (user == null) {
			logger.info("User (id = " + id + ") doesn't exist.");
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/user/{name}", method = RequestMethod.DELETE)
	public void removePerson(@PathVariable("name") String name) {
		this.userService.deleteUserByName(name);
	}

}
