<%@page session="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Manage User</title>
</head>

<body>

	<p>Time ${serverTime}.</p>
	<br />

	<p>You can only update your password and address information.</p>
	<p>User name cannot be changed in this system.</p>
	<h1>Update User ${name}</h1>
	<form:form action="update" method="post"
		modelAttribute="updateForm">
		<input type="hidden" name="name" />
		<br />
		Password:&nbsp;<input type="password" name="password" />
		<br />
		<br />
		Address:&nbsp;<input type="text" name="address" />
		<br />
		<br />
		<input type="submit" value="Submit" />
	</form:form>
	<br />
	<br />
</body>

</html>
