package com.apple.webapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;

/**
 * Handles requests for the application home page.
 */
@Entity
@Table(name = "T_USER")
@Scope("prototype")
public class User {

	@Id
	@Column(name = "name")
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	private String name;
	private String password;
	private String address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", password=" + password + ", address=" + address + "]";
	}

}
