package com.apple.webapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.apple.webapp.dao.UserDAO;
import com.apple.webapp.model.User;
import com.apple.webapp.service.UserService;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	@Transactional
	public User findUserByName(String name) {
		return this.userDAO.findUserByName(name);
	}

	@Override
	@Transactional
	public void saveUser(User user) {
		this.userDAO.saveUser(user);
	}

	@Override
	@Transactional
	public void updateUser(User user) {
		this.userDAO.updateUser(user);
	}

	@Override
	@Transactional
	public void deleteUserByName(String name) {
		this.userDAO.deleteUserByName(name);
	}

	@Override
	@Transactional
	public boolean isUserExist(User user) {
		return this.userDAO.isUserExist(user.getName());
	}

	@Override
	@Transactional
	public void login(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional
	public List<User> findAllUsers() {
		return this.userDAO.findAllUsers();
	}

}
