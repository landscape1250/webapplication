package com.apple.webapp.service;

import java.util.List;

import com.apple.webapp.model.User;

public interface UserService {

	User findUserByName(String name);

	void saveUser(User user);

	void updateUser(User user);
	
	void login(User user);

	void deleteUserByName(String name);

	List<User> findAllUsers();

	public boolean isUserExist(User user);

}
