package com.apple.webapp.dao;

import java.util.List;

import com.apple.webapp.model.User;

public interface UserDAO {

	void saveUser(User user);

	User findUserByName(String name);

	List<User> findAllUsers();

	User updateUser(User user);

	void deleteUserByName(String name);

	boolean isUserExist(String name);

}
