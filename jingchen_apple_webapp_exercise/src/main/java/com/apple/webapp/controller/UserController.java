package com.apple.webapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.apple.webapp.model.User;
import com.apple.webapp.service.UserService;

/**
 * Handles requests for the application home page.
 */

@RequestMapping("/")
@Controller
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	private static final String CREATE = "create";
	private static final String MANAGE = "manage";

	@Autowired
	private UserService userService;

	/**
	 * Create a User
	 */
	@RequestMapping(value = "user", method = RequestMethod.POST)
	public String createUser(@ModelAttribute("createForm") User user) {
		logger.info("Create User " + user.getName());
		if (!userService.isUserExist(user)) {
			userService.saveUser(user);
			return "create";
		} else {
			if (logger.isInfoEnabled()) {
				logger.info("Sorry, User " + user.getName() + " already exist");
			}
			return CREATE;
		}
	}

	/**
	 * Log in
	 */
	@RequestMapping(value = "user/login", method = RequestMethod.POST)
	public String login(@ModelAttribute("loginForm") User user) {
		logger.info("Log in " + user.getName());
		if (!userService.isUserExist(user)) {
			if (logger.isInfoEnabled()) {
				logger.info("Sorry, User " + user.getName() + " doesn't exist");
			}
			return CREATE;
		} else {
			if (logger.isInfoEnabled()) {
				logger.info("User " + user.getName() + " logged in");
			}
			return MANAGE;
		}
	}

	/**
	 * Find One User By Name
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/user/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> findUserByName(@PathVariable("name") String name) {
		if (name == null || name.trim().isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("Please input valid name.");
			}
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		User user = userService.findUserByName(name);
		if (user == null) {
			logger.info("User (name = " + name + ") doesn't exist.");
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/**
	 * Update a User
	 */
	@RequestMapping(value = "user/update", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("updateForm") User user) {
		logger.info("Update User " + user.getName());
		this.userService.updateUser(user);
		if (logger.isInfoEnabled()) {
			logger.info("User " + user.getName() + " information updated");
		}
		return MANAGE;
	}

	/**
	 * Remove user
	 * 
	 * @param name
	 */
	@RequestMapping(value = "/user/{name}", method = RequestMethod.DELETE)
	public void removePerson(@PathVariable("name") String name) {
		this.userService.deleteUserByName(name);
	}

}
