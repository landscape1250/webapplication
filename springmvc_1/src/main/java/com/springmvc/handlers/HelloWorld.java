package com.springmvc.handlers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.springmvc.util.Status;

@Controller
public class HelloWorld {

	/**
	 * /WEB-INF/views/success.jsp
	 * 
	 * @return
	 */
	@RequestMapping("/helloworld")
	public String hello() {
		System.out.println("Hello World !");
		// return Status.SUCCESS.name();
		return "success";
	}
}
