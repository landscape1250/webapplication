package com.springmvc.handlers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springmvc.model.User;

@RequestMapping("springmvc")
@Controller
public class SpringMVCTest {

	private static final String SUCCESS = "success";

	/**
	 * @RequestMapping
	 * @return
	 */
	@RequestMapping("test-request-mapping")
	public String testRequestMapping() {
		System.out.println("invoke testRequestMapping");
		return SUCCESS;
	}

	/**
	 * RequestMethod
	 * 
	 * @return
	 */
	@RequestMapping(value = "test-post-request", method = RequestMethod.POST)
	public String testPostRequest() {
		System.out.println("invoke testPostRequest");
		return SUCCESS;
	}

	/**
	 * value params
	 * 
	 * @return
	 */
	@RequestMapping(value = "test-params", params = { "username", "age!=10" })
	public String testParams() {
		System.out.println("invoke testParams");
		return SUCCESS;
	}

	/**
	 * @PathVariable
	 * @param id
	 * @return
	 */
	@RequestMapping("test-path-variable/{id}")
	public String testPathVariable(@PathVariable("id") Integer id) {
		System.out.println("invoke testPathVariable: " + id);
		return SUCCESS;
	}

	/**
	 * @RequestParam
	 * @param name
	 * @param age
	 * @return
	 */
	@RequestMapping("test-request-param")
	public String testRequestParam(@RequestParam(value = "username") String name,
			@RequestParam(value = "age", required = false, defaultValue = "0") int age) {
		System.out.println("invoke testRequestParam: " + "username: " + name + ", age: " + age);
		return SUCCESS;
	}

	/**
	 * @RequestHeader
	 * @param encoding
	 * @param language
	 * @return
	 */
	@RequestMapping("test-request-header")
	public String testRequestHeader(@RequestHeader(value = "Accept-Encoding") String encoding,
			@RequestHeader(value = "Accept-Language") String language) {
		System.out.println(
				"invoke testRequestHeader: " + "\nAccept-Encoding: " + encoding + "\nAccept-Language: " + language);
		return SUCCESS;
	}

	/**
	 * @CookieValue
	 * @param sessionId
	 * @return
	 */
	@RequestMapping("test-cookie")
	public String testCookieValue(@CookieValue("JSESSIONID") String sessionId) {
		System.out.println("invoke testCookieValue: " + sessionId);
		return SUCCESS;
	}

	/**
	 * POJO
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping("test-pojo")
	public String testPOJORequest(User user) {
		System.out.println("User: " + user);
		return SUCCESS;
	}

}
