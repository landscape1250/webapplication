<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Hello world!</h1>

	<P>The time on the server is ${serverTime}.</P>
	<a href="helloworld">Hello World from Spring MVC.</a>
	<br />
	<br />
	<a href="springmvc/test-request-mapping"> Test Spring MVC Request
		Mapping</a>
	<br />
	<br />

	<form action="springmvc/test-post-request" method="post">
		<input type="submit" value="Submit">
	</form>

	<a href="springmvc/test-params?username=smith&age=10">Test
		Parameters Request</a>
	<br />
	<br />
	<a href="springmvc/test-params?username=smith&age=12">Test
		Parameters Request 2</a>
	<br />
	<br />
	<a href="springmvc/test-path-variable/1">Test Path variable Request
	</a>
	<br />
	<br />
	<a href="springmvc/test-request-param?username=John&age=9">Test
		Request Parameter </a>
	<br />
	<br />
	<a href="springmvc/test-request-header">Test Request Header</a>
	<br />
	<br />
	<a href="springmvc/test-cookie">Test Cookie</a>
	<br />
	<br />
	<form action="springmvc/test-pojo" method="post">
		User name:&nbsp;<input type="text" name="username" /><br />
		Password:&nbsp;<input type="password" name="password" /><br />
		Age:&nbsp;<input type="text" name="age" /><br /> Email:&nbsp; <input
			type="text" name="email" /><br /> City:&nbsp; <input type="text"
			name="address.city" /><br /> State:&nbsp;<input type="text"
			name="address.state" /><br /> Country:&nbsp;<input type="text"
			name="address.country" /><br /> Zip code: &nbsp;<input type="text"
			name="address.zipcode"><br /> <input type="submit"
			value="Submit" />
	</form>


</body>
</html>
